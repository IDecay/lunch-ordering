class DishesController < ApplicationController
  include OrdersHelper
  before_action :authenticate_user!, except: [:index, :show]
  load_and_authorize_resource
  skip_authorize_resource only: [:index, :show]
  before_action :set_dish, only: [:show, :edit, :update, :destroy, :add_to_cart, :remove_from_cart]

  DISHES_PER_PAGE = 10

  # GET /dishes
  # GET /dishes.json
  def index
    @dishes = Dish.all
    @weekdays = Dish.day_of_the_weeks.keys.delete_if { |key| key == 'weekday_not_specified' }
    dish_filter = params[:dish_filter]
    if !dish_filter.nil? && dish_filter.is_a?(String) && !dish_filter.empty?
      dish_filter.downcase!
      if @weekdays.include? dish_filter
        @dishes = @dishes.weekday(dish_filter.to_sym)
      elsif dish_filter == 'today'
        @dishes = @dishes.today
      end
    end
    page = params[:page].is_a?(String) && params[:page].is_positive_i? ? params[:page].to_i : 1
    @dishes = @dishes.paginate(per_page: DISHES_PER_PAGE, page: page)
  end

  # GET /dishes/1
  # GET /dishes/1.json
  def show
  end

  # GET /dishes/new
  def new
    @dish = Dish.new
  end

  # POST /dishes/add/:id
  def add_to_cart
    @order = current_order
    if @order.validate_new_dish(@dish) && @order.valid?
      @order.dishes << @dish
      @order.save
      session[:order_id] = @order.id
    end
    #redirect_to dishes_url
  end

  # POST /dishes/remove/:id
  def remove_from_cart
    @order = current_order
    @order.dishes.delete(@dish)
    if @order.save
      session[:order_id] = @order.id
    end
    #redirect_to edit_order_url(@order)
  end

  def create
    @dish = Dish.new(dish_params)
    if (@dish.weekday_not_specified? || @dish.day_of_the_week.nil?) && @dish.single_day.nil?
      @dish.single_day = Date.today
    end
    respond_to do |format|
      if @dish.save
        format.html { redirect_to @dish, notice: 'Dish was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /dishes/1
  # PATCH/PUT /dishes/1.json
  def update
    respond_to do |format|
      if params[:dish][:remove_picture] == 1
        @dish.remove_picture!
      end
      if @dish.update(dish_params)
        format.html { redirect_to @dish, notice: 'Dish was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /dishes/1
  # DELETE /dishes/1.json
  def destroy
    @dish.destroy
    respond_to do |format|
      format.html { redirect_to dishes_url, notice: 'Dish was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_dish
    @dish = Dish.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def dish_params
    params.require(:dish).permit(:name, :picture, :dish_type, :day_of_the_week, :price, :single_day, :remove_picture)
  end
end
