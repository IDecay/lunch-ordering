class OrdersController < ApplicationController
  include ApplicationHelper
  include OrdersHelper
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_current_order, only: [:new, :create, :update]
  before_action :set_order, only: [:show, :destroy]
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.includes(:user).index_filtering
    date = params[:date]
    if !date.nil? && !date.empty?
      date = date.to_date
      @orders = @orders.filter_by_date(date)
    end
    if not_admin?
      @orders = @orders.not_admin(current_user.id)
    else
      @summary_price = @orders.to_a.sum(&:total_price)
    end
    page = params[:page].is_a?(String) && params[:page].is_positive_i? ? params[:page].to_i : 1
    @orders = @orders.paginate(per_page: 10, page: page)
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
  end

  # POST /orders
  # POST /orders.json
  def create
    create_order
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    create_order
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_current_order
    @order = current_order
  end

  def set_order
    @order = Order.includes(:dishes, :user).find(params[:id])
  end

  def create_order
    @order.status = :created
    respond_to do |format|
      if @order.save
        session.delete(:order_id)
        format.html { redirect_to dishes_url, notice: 'Order was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end
end
