module Api
  class OrdersController < ApiController
    load_and_authorize_resource :order
    def index
      @orders = Order.includes(:dishes, :user).all.index_filtering.filter_by_date(Date.today)
      json_response(OrderSerializer.new(@orders, include: [:dishes]))
    end
  end
end