class Order < ApplicationRecord
  has_and_belongs_to_many :dishes, -> { with_deleted }
  belongs_to :user
  enum status: [ :draft, :created, :in_process, :delivered ]

  scope :index_filtering, -> { where('status != ?', statuses[:draft]).order(created_at: :desc) }
  scope :not_admin, ->(user_id) { where(user_id: user_id) }
  scope :filter_by_date, ->(date) { where(['created_at >= ? AND created_at < ?', date, date + 1]) }

  validates :user_id, presence: true
  validates :status, presence: true
  validate :has_one_of_each_dish_type, :is_available_today

  ONE_OF_EACH_TYPE_ERROR = 'You need to choose 1 first course, 1 main course and 1 drink'

  def total_price
    return 0 if dishes.blank?
    dishes.sum(:price)
  end

  def validate_new_dish(dish)
    validate_dish_type(dish) && validate_dish_availability(dish)
  end

  private

  def validate_dish_type(dish)
    if !dishes.nil? && dishes.dish_type(dish.dish_type).any?
      errors[:base] << ONE_OF_EACH_TYPE_ERROR
      return false
    end
    return true
  end

  def validate_dish_availability(dish)
    unless dish.available_today?
      errors[:base] << "#{dish.name} is not available today"
      return false
    end
    return true
  end

  def has_one_of_each_dish_type
    if (dishes.empty? || dishes.length != 3) && !draft?
      errors[:base] << ONE_OF_EACH_TYPE_ERROR
      return
    end

    if draft?
      unless dishes.drinks.size <= 1 && dishes.first_courses.size <= 1 && dishes.second_courses.size  <= 1
        errors[:base] << ONE_OF_EACH_TYPE_ERROR
      end
    else
      unless dishes.drinks.size == 1 && dishes.first_courses.size == 1 && dishes.second_courses.size == 1
        errors[:base] << ONE_OF_EACH_TYPE_ERROR
      end
    end
  end

  def is_available_today
    dishes.not_today.each do |dish|
      errors[:base] << "#{dish.name} is not available today"
    end
  end
end
