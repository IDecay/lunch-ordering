class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :user
      can [:add_to_cart, :remove_from_cart, :read], Dish
      can :create, Order
      can :read, Order do |order|
        order.user_id == user.id && order.created?
      end
      can [:read, :update], Order do |order|
        order.user_id == user.id && order.draft?
      end
      can [:read, :update], User, id: user.id
    elsif user.has_role? :admin
      can :manage, :all
    end

    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
