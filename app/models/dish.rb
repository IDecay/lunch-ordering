class Dish < ApplicationRecord
  acts_as_paranoid
  has_and_belongs_to_many :orders
  enum day_of_the_week: [ :sunday, :monday, :tuesday, :wendesday, :thursday, :friday, :saturday, :weekday_not_specified ]
  enum dish_type: [ :first_course, :second_course, :drink ]
  mount_uploader :picture, PictureUploader

  default_scope -> { order(dish_type: :asc) }
  scope :weekday, ->(weekday) { where(day_of_the_week: day_of_the_weeks[weekday]) }
  scope :dish_type, ->(dish_type) { where(dish_type: dish_types[dish_type]) }
  scope :today, -> { where('day_of_the_week = ? OR single_day = ?', Date.today.wday, Date.today) }
  scope :not_today, -> { where('day_of_the_week != ? AND (single_day is null OR single_day != ?)', Date.today.wday, Date.today) }
  scope :drinks, -> { dish_type(:drink) }
  scope :first_courses, -> { dish_type(:first_course) }
  scope :second_courses, -> { dish_type(:second_course) }

  validates :name, presence: true
  validates :dish_type, presence: true
  validates :price, presence: true, numericality: { greater_than: 0 }
  validate  :picture_size, :day_selected, :single_day_not_in_past

  def available_today?
    Dish.day_of_the_weeks[day_of_the_week] == Date.today.wday || single_day == Date.today
  end

  private

  def picture_size
    return if picture.nil? || picture.blank?
    if picture.size > 5.megabytes
      errors.add(:picture, 'should =>  be less than 5MB')
    end
  end

  def day_selected
    if (day_of_the_week.nil? || weekday_not_specified?) && single_day.nil?
      errors[:base] << 'Specify day of the week or single day when this dish is available for users'
    end
  end

  def single_day_not_in_past
    return if single_day.nil? || single_day.blank? || !weekday_not_specified?
    if single_day.past?
      errors.add(:single_day, 'cannot be in past')
    end
  end
end
