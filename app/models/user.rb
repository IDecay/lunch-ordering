class User < ApplicationRecord
  has_many :orders
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  before_create :assign_role
  validates :name, presence: true

  def assign_role
    default_role = User.all.blank? ? :admin : :user
    add_role(default_role) if roles.blank?
  end
end
