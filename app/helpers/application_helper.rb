module ApplicationHelper
  def admin?
    user_signed_in? && current_user.has_role?(:admin)
  end

  def not_admin?
    user_signed_in? && !current_user.has_role?(:admin)
  end
end

