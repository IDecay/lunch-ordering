module OrdersHelper
  def current_order
    if session[:order_id]
      Order.find(session[:order_id])
    else
      order = Order.new
      order.user = current_user
      order.status = :draft
      order
    end
  end

end
