class DishSerializer
  include FastJsonapi::ObjectSerializer
  set_type :dish
  attributes :name, :price, :dish_type
end
