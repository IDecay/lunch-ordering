class OrderSerializer
  include FastJsonapi::ObjectSerializer
  set_type :order
  attributes :created_at, :status, :user_id

  attribute :total_price do |order|
    order.total_price.round(2)
  end

  has_many :dishes
end
