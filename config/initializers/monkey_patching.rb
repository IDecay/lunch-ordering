module CoreExtensions
  module String
    module Validation
      def is_positive_i?
        /\A[+]?\d+\z/ === self
      end
    end
  end
end

String.include CoreExtensions::String::Validation