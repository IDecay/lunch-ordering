Rails.application.routes.draw do
  devise_for :users
  resources :dishes do
    member do
      post 'add_to_cart', as: 'add'
      post 'remove_from_cart', as: 'remove'
    end
  end
  resources :orders
  resources :users, only: ['index', 'show']

  namespace :api do
    post 'authenticate', to: 'authentication#authenticate'
    post 'orders', to: 'orders#index'
  end

  root 'dishes#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
