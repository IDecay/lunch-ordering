require 'rails_helper'

RSpec.feature 'Create order', type: :feature do
  before :each do
    @admin = User.all.first || create(:user)
    @user = create(:user)
    sign_in(@user)
    @dishes = []
    3.times do
      @dishes.push(create(:dish))
    end
  end

  after :each do
    sign_out
  end

  it 'with 3 different dishes', js: true do
    fill_cart(@dishes)
    click_on 'Cart'
    click_on 'Submit'
    expect(page).to have_text('Order was successfully created.')
  end

  it 'with 2 different dishes', js: true do
    fill_cart(@dishes)
    click_on 'Cart'

    @dishes.each do |dish|
      expect(page.find("tr[data-id='#{dish.id}']")).to have_link('Remove from cart')
    end

    first_dish = @dishes.first
    page.find("tr[data-id='#{first_dish.id}']").find('a', text: 'Remove from cart').click
    click_on 'Submit'
    expect(page).to have_selector('#error_explanation')
  end

  it 'with duplicate dish types', js: true do
    visit '/dishes'
    click_on 'Today'

    first_dish = @dishes.first
    expect(page.find("tr[data-id='#{first_dish.id}']")).to have_link('Add to cart')
    page.find("tr[data-id='#{first_dish.id}']").find('a', text: 'Add to cart').click
    expect(page).to have_text('was added to your cart')
    page.find("tr[data-id='#{first_dish.id}']").find('a', text: 'Add to cart').click
    expect(page).to have_selector('#error_explanation')
  end
end