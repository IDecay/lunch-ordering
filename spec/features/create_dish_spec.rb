require 'rails_helper'

RSpec.feature "Create dish", type: :feature do
  before :each do
    @admin = User.all.first || create(:user)
    sign_in(@admin)
  end

  after :each do
    sign_out
  end

  it 'with valid params' do
    click_on 'Dishes'
    click_on 'New Dish'

    fill_in 'Name', with: 'TestDish'
    fill_in 'Price', with: 10.00

    click_button 'Save'
    expect(page).to have_text('Dish was successfully created.')
  end

  it 'with invalid params' do
    click_on 'Dishes'
    click_on 'New Dish'

    fill_in 'Name', with: 'WrongDish'
    fill_in 'Price', with: 10.00
    fill_in 'Single day', with: Date.today - 2

    click_button 'Save'
    expect(page).to have_text('prohibited this dish from being saved')
  end
end
