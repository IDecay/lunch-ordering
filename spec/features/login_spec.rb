require 'rails_helper'

RSpec.feature "Login", type: :feature do
  before :all do
    @user = create(:user)
  end
  it 'signs in with valid credentials' do
    sign_in(@user)
    expect(page).to have_text('Signed in successfully.')
    sign_out
    expect(page).to have_text('Signed out successfully.')
  end

  it 'signs in with invalid credentials' do
    sign_in(@user, false)
    expect(page).to have_text('Invalid Email or password.')
  end
end
