require 'rails_helper'

RSpec.describe "orders/_orders", type: :view do
  context "with 2 orders" do
    before(:each) do
      @orders = [
          create(:created_order),
          create(:created_order)
      ]
    end

    it "displays 2 orders from list" do
      toggle_user_view(false)
      render partial: "orders/orders.html.haml", locals: { orders: @orders }

      expect(rendered).to match /Created at/
      expect(rendered).to match /Summary price/
      expect(rendered).to match /State/
      expect(rendered).to match /Show/

      expect(rendered).not_to match /Username/
      expect(rendered).not_to match /Destroy/
    end

    it "displays 2 orders from list for admin" do
      toggle_user_view(true)
      render partial: "orders/orders.html.haml", locals: { orders: @orders }

      expect(rendered).to match /Created at/
      expect(rendered).to match /Summary price/
      expect(rendered).to match /State/
      expect(rendered).to match /Show/
      expect(rendered).to match /Username/
      expect(rendered).to match /Destroy/
    end
  end

end
