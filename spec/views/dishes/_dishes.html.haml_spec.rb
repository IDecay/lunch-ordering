require 'rails_helper'

RSpec.describe "dishes/_dishes", type: :view do
  context "with 3 dishes" do
    before(:each) do
      @dishes = [
        create(:dish, name: 'Dish1'),
        create(:dish, name: 'Dish2'),
        create(:dish, name: 'Dish3')
      ]
    end

    it "displays 3 dishes from list" do
      toggle_user_view(false)
      render partial: "dishes/dishes.html.haml", locals: { dishes: @dishes, is_order: false }

      expect(rendered).to match /Dish1/
      expect(rendered).to match /Dish2/
      expect(rendered).to match /Dish3/
      expect(rendered).to match /Show/

      expect(rendered).to match /Add to cart/
      expect(rendered).not_to match /Remove from cart/
      expect(rendered).not_to match /Edit/
      expect(rendered).not_to match /Destroy/
    end

    it "displays 3 dishes from order" do
      toggle_user_view(false)
      render partial: "dishes/dishes.html.haml", locals: { dishes: @dishes, is_order: true }

      expect(rendered).to match /Dish1/
      expect(rendered).to match /Dish2/
      expect(rendered).to match /Dish3/

      expect(rendered).to match /Remove from cart/
      expect(rendered).not_to match /Add to cart/
      expect(rendered).not_to match /Edit/
      expect(rendered).not_to match /Destroy/
    end

    it "displays 3 dishes from list for admin" do
      toggle_user_view(true)
      render partial: "dishes/dishes.html.haml", locals: { dishes: @dishes, is_order: false }

      expect(rendered).to match /Dish1/
      expect(rendered).to match /Dish2/
      expect(rendered).to match /Dish3/

      expect(rendered).to match /Show/
      expect(rendered).to match /Edit/
      expect(rendered).to match /Destroy/

      expect(rendered).not_to match /Add to cart/
      expect(rendered).not_to match /Remove from cart/
    end
  end

end
