require 'rails_helper'

RSpec.describe "users/index", type: :view do
  it "displays all the users" do
    assign(:users, [
      create(:user),
      create(:user)
    ])

    render

    expect(rendered).to match /Registered at/
    expect(rendered).to match /Name/
    expect(rendered).to match /Email/
  end
end
