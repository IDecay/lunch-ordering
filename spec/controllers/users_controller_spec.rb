require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  before :all do
    @admin = User.all.first || create(:user)
    @user = create(:user)
    @another_user = create(:user)
  end
  before :each do
    bypass_rescue
  end
  context 'admin' do
    before :each do
      stub_current_user(@admin)
    end

    describe "GET #index" do
      it "returns http success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      context "personal account" do
        it "returns a success response" do
          get :show, params: { id: @admin.to_param }
          expect(response).to have_http_status(:success)
        end
      end

      context "another user's account" do
        it "returns a success response" do
          get :show, params: { id: @another_user.to_param }
          expect(response).to have_http_status(:success)
        end
      end
    end
  end

  context 'user' do
    before :each do
      stub_current_user(@user)
    end

    describe "GET #show" do
      context "personal account" do
        it "returns a success response" do
          get :show, params: { id: @user.to_param }
          expect(response).to have_http_status(:success)
        end
      end

      context "another user's account" do
        it "returns a success response" do
          expect {
            get :show, params: { id: @another_user.to_param }
          }.to raise_error(CanCan::AccessDenied)
        end
      end
    end
  end

  context 'anonymous' do
    describe "GET #show" do
      context "another user's account" do
        it "returns a success response" do
          get :show, params: { id: @another_user.to_param }
          expect(response).to redirect_to(new_user_session_url)
        end
      end
    end
  end

end
