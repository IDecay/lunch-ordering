require 'rails_helper'
require 'jwt'
describe Api::OrdersController, type: :api do
  context 'POST api/orders' do
    before do
      header 'Content-type', 'application/json'
    end
    it 'has no token' do
      post 'api/orders'
      expect(status).to eq 401
      message = json['error']
      expect(message).to eq('Not Authorized')
    end
    it 'has invalid token' do
      @user = create(:user)
      token = JsonWebToken.encode({user_id: @user.id})
      header 'Authorization', "Bearer #{token + 'random_string'}"
      post 'api/orders'
      expect(status).to eq 401
      message = json['error']
      expect(message).to eq('Not Authorized')
    end
    it 'responds with 200' do
      @user = create(:user)
      token = JsonWebToken.encode({user_id: @user.id})
      header 'Authorization', "Bearer #{token}"
      post 'api/orders'
      expect(status).to eq 200
    end
  end
end
