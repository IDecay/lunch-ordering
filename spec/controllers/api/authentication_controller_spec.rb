require 'rails_helper'
require 'jwt'
describe Api::AuthenticationController, type: :api do
  context 'POST api/authenticate' do
    before do
      @user = create(:user)
      header 'Content-type', 'application/json'
    end
    it 'has no credentials' do
      post 'api/authenticate'
      expect(status).to eq 401
      message = json['error']['user_authentication'].first
      expect(message).to eq('invalid credentials')
    end
    it 'has invalid credentials' do
      post 'api/authenticate', { email: @user.email, password: DEFAULT_PASSWORD + 'random_string' }.to_json
      expect(status).to eq 401
      message = json['error']['user_authentication'].first
      expect(message).to eq('invalid credentials')
    end
    it 'responds with 200' do
      post 'api/authenticate', { email: @user.email, password: DEFAULT_PASSWORD }.to_json
      expect(status).to eq 200
      token = json['auth_token']
      expect(token.nil?).to be false
    end
  end
end