DEFAULT_PASSWORD = 'password1'

FactoryBot.define do
  sequence(:count)

  factory :user do
    sequence(:name) { |n| "User#{n}" }
    sequence(:email) { Faker::Internet.email }
    password DEFAULT_PASSWORD
    password_confirmation DEFAULT_PASSWORD
  end

  factory :dish do
    name { Faker::Food.dish }
    price { Faker::Number.decimal(2, 2) }
    sequence(:dish_type) { |n| n % Dish.dish_types.count }
    single_day { Date.today }

    factory :dish_without_day do
      single_day nil
      day_of_the_week :weekday_not_specified
    end

    factory :invalid_dish do
      name nil
      price nil
    end
  end

  factory :order do
    created_at { DateTime.now }
    user { create(:user) }
    status :draft

    factory :order_with_dishes do
      before(:create) do |order|
        first = create(:dish, dish_type: :first_course)
        second = create(:dish, dish_type: :second_course)
        drink = create(:dish, dish_type: :drink)
        order.dishes << first
        order.dishes << second
        order.dishes << drink
      end

      factory :created_order do
        after(:create) do |order|
          order.status = :created
        end
      end
    end
  end
end