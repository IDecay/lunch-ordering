module ViewHelpers
  def toggle_user_view(is_admin)
    allow(view).to receive(:not_admin?).and_return(!is_admin)
    allow(view).to receive(:admin?).and_return(is_admin)
  end
end