module FeatureHelpers
  def sign_in(user, valid=true)
    visit '/'
    click_link 'Log in'
    fill_in 'Email', with: user.email
    fill_in 'Password', with: DEFAULT_PASSWORD + (valid ? '' : 'random_string')
    click_button 'Log in'
  end

  def sign_out
    click_on 'Account'
    click_on 'Log out'
  end

  def fill_cart(dishes)
    visit '/dishes'
    click_on 'Today'

    dishes.each do |dish|
      expect(page.find("tr[data-id='#{dish.id}']")).to have_link('Add to cart')
      page.find("tr[data-id='#{dish.id}']").find('a', text: 'Add to cart').click
      expect(page).to have_text('was added to your cart')
    end
    expect(page).not_to have_selector('#error_explanation')

  end
end