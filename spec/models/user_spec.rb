require 'rails_helper'
require 'cancan'
require 'cancan/matchers'
require_relative '../../app/models/ability.rb'

RSpec.describe User, type: :model do
  context 'valid Factory' do
    it 'has a valid factory' do
      expect(build(:user)).to be_valid
    end
  end

  context 'validations' do
    context 'presence' do
      it 'has no name' do
        expect(build(:user, name: nil)).to be_invalid
      end
      it 'has no email' do
        expect(build(:user, email: nil)).to be_invalid
      end
    end

    context 'uniqueness' do
      before do
        @user = create(:user)
        @another_user = create(:user)
        @another_user.email = @user.email
      end
      it 'has not unique email' do
        expect(@user).to be_valid
        expect(@another_user).to be_invalid
      end
    end

    it 'has valid password' do
      expect(build(:user).valid_password?(DEFAULT_PASSWORD)).to be true
    end
  end

  context 'password confirmation' do
    before { @user = build(:user, password: DEFAULT_PASSWORD, password_confirmation: DEFAULT_PASSWORD + '1') }
    it 'has invalid confirmation' do
      expect(@user).to_not be_valid
    end
  end

  context 'roles validation' do
    before do
      Role.create(name: 'admin')
      Role.create(name: 'user')
      @admin = User.all.first || create(:user)
      @user = create(:user)
    end
    it 'has admin role' do
      expect(@admin.has_role?(:admin)).to be true
    end
    it 'has user role' do
      expect(@user.has_role?(:user)).to be true
    end
  end

  describe 'abilities' do
    before(:all) do
      @admin = User.all.first || create(:user)
      @user = create(:user)
      @another_user = create(:user)
      @admin_ability = Ability.new(@admin)
      @user_ability = Ability.new(@user)
    end

    context 'dish operations' do
      context 'admin' do
        it 'should be able to manage users' do
          @admin_ability.should be_able_to(:manage, Dish.new)
        end
      end

      context 'user' do
        it 'should be able to read information about dish, add and remove it from cart' do
          @user_ability.should be_able_to([:read, :add_to_cart, :remove_from_cart], Dish.new)
          @user_ability.should_not be_able_to([:destroy, :create, :update], Dish.new)
        end
      end
    end

    context 'order operations' do
      before(:all) do
        @user_order = create(:order_with_dishes)
        @user_order.user = @user
        @user_order.save
        @another_user_order = create(:order_with_dishes)
        @another_user_order.user = @another_user
        @another_user_order.save
      end
      context 'admin' do
        it 'should be able to manage orders' do
          @admin_ability.should be_able_to(:manage, @user_order)
        end
      end

      context 'user' do
        it 'should be able to update his order only before it was submitted' do
          @user_ability.should be_able_to([:create, :read, :update], @user_order)
          @user_ability.should_not be_able_to([:destroy], @user_order)
          @user_ability.should_not be_able_to([:create, :read, :update, :destroy], @another_user)
          @user_order.status = :created
          @user_order.save
          @user_ability.should be_able_to(:read, @user_order)
          @user_ability.should_not be_able_to(:update, @user_order)
        end
      end
    end

    context 'account operations' do
      context 'admin' do
        it 'should be able to manage users' do
          @admin_ability.should be_able_to(:manage, @user)
        end
      end

      context 'user' do
        it 'should be able to manage his own account' do
          @user_ability.should be_able_to([:read, :update], @user)
          @user_ability.should_not be_able_to([:destroy, :read, :update], @another_user)
        end
      end
    end
  end
end
