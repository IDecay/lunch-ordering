require 'rails_helper'

RSpec.describe Dish, type: :model do
  context 'valid Factory' do
    it 'has a valid factory' do
      expect(build(:dish)).to be_valid
      expect(build(:dish_without_day)).to be_invalid
    end
  end

  context 'validations' do
    context 'presence' do
      it 'has no weekday and no single day specified' do
        expect(build(:dish_without_day)).to be_invalid
      end
      it 'has no name' do
        expect(build(:dish, name: nil)).to be_invalid
      end
      it 'has no price' do
        expect(build(:dish, price: nil)).to be_invalid
      end
      it 'has no dish_type' do
        expect(build(:dish, dish_type: nil)).to be_invalid
      end
    end

    context 'other validations' do
      it 'has single day specified in the past' do
        expect(build(:dish, single_day: Date.today - 1)).to be_invalid
      end
      it 'has positive price' do
        expect(build(:dish, price: 0)).to be_invalid
        expect(build(:dish, price: -1)).to be_invalid
      end
    end
  end
end
