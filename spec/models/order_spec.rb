require 'rails_helper'

RSpec.describe Order, type: :model do
  before(:each) do
    @order = create(:created_order)
  end

  context 'valid Factory' do
    it 'has a valid factory' do
      expect(create(:order)).to be_valid
      expect(create(:order_with_dishes)).to be_valid
      expect(@order).to be_valid
    end
  end

  context 'validations' do
    context 'presence' do
      it 'requires user' do
        @order.user = nil
        expect(@order).to be_invalid
      end

      it 'requires dishes' do
        @order.dishes = []
        expect(@order).to be_invalid
      end
    end

    context 'other validations' do
      it 'has only available dishes' do
        incorrect_dish = @order.dishes.first
        incorrect_dish.single_day = nil
        incorrect_dish.day_of_the_week = (Date.today.wday + 2) % 7
        incorrect_dish.save
        expect(@order).to be_invalid
      end

      it 'has only one of each type of dish (draft)' do
        @draft = create(:order_with_dishes)
        @draft.dishes.first.first_course!
        @draft.dishes.last.first_course!
        expect(@draft).to be_invalid
      end

      it 'validates order before dish added' do
        @draft = create(:order)
        @dish = create(:dish)
        @duplicate_dish = @dish.dup
        expect(@draft.validate_new_dish(@dish)).to be true
        @draft.dishes << @dish
        @draft.save
        @duplicate_dish.save
        expect(@draft.validate_new_dish(@duplicate_dish)).to be false
      end

      it 'has only one of each type of dish' do
        @order.dishes.first.first_course!
        @order.dishes.last.first_course!
        expect(@order).to be_invalid
      end
    end
  end
end
