https://stark-thicket-37912.herokuapp.com/
To test admin features login under credentials: admin@mail.ru / 123123

##### Prerequisites

The setups steps expect following tools installed on the system.

- Gitlab
- Ruby [2.5.1]
- Rails [5.2.0]

##### 1. Check out the repository

```bash
git clone https://gitlab.com/IDecay/lunch-ordering.git
```

##### 2. Change database.yml file

Edit the database.yml file and set database configuration as required.


##### 3. Create and setup the database

Run the following command to create and setup the database.

```ruby
bundle exec rails db:create
bundle exec rails db:setup
```

##### 4. Start the Rails server

You can start the rails server using the command given below.

```ruby
bundle exec rails s
```

And now you can visit the site with the URL http://localhost:3000