class ChangeDishesDayOfTheWeek < ActiveRecord::Migration[5.2]
  def change
    change_column :dishes, :day_of_the_week, :integer, default: 7
  end
end
