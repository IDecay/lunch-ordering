class AddDeletedAtToDishes < ActiveRecord::Migration[5.2]
  def change
    add_column :dishes, :deleted_at, :datetime
  end
end
