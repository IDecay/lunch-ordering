class CreateDishes < ActiveRecord::Migration[5.2]
  def change
    create_table :dishes do |t|
      t.string :name
      t.decimal :price
      t.date :single_day
      t.integer :day_of_the_week
      t.integer :dish_type
      t.string :picture

      t.timestamps
    end
  end
end
