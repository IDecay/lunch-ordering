class CreateJoinTableDishOrder < ActiveRecord::Migration[5.2]
  def change
    create_join_table :dishes, :orders do |t|
      t.index :order_id
    end
  end
end
