100.times do
  Dish.create(
    name: Faker::Food.dish,
    price: Faker::Number.decimal(2, 2),
    single_day: Faker::Date.between(Date.today, 1.months.from_now),
    day_of_the_week: [ :sunday, :monday, :tuesday, :wendesday, :thursday, :friday, :saturday ].sample,
    dish_type: [ :first_course, :second_course, :drink ].sample
  )
end